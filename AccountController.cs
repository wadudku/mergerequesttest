﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using IDMS.Entity.Models;
using IDMS.Entity.ServiceModels;
using IDMS.Entity.ViewModels;
using IDMS.Repository;
using IDMS.Utility;
using IDMS.Utility.StaticData;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using System.IO;


namespace IDMS.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = CommonData.AllScreens)]
    public class AccountController : ControllerBase
    {
        private readonly IUserRepository _repository;
        private readonly AuthConfiguration _authConfiguration;
        private readonly ILogError _logError;
        private readonly IWebHostEnvironment _env;

        public AccountController(IUserRepository repository,
            IOptions<AuthConfiguration> configuration,
            IWebHostEnvironment env,
            ILogError logError)
        {
            _repository = repository;
            _authConfiguration = configuration.Value;
            _logError = logError;
            _env = env;
        }

        [HttpGet]
        public async Task<ListResult<User>> Get()
        {
            var result = new ListResult<User>();
            try
            {
                result.Data = await _repository.CollectUserInfos()
                    .Select(x => new User
                    {
                        Id = x.Id,
                        Username = x.Username,
                        UserScreens = x.UserScreens,
                        IsActive = x.IsActive
                    })
                    .ToListAsync();

                return result;
            }
            catch (Exception exp)
            {
                // keep log
                _logError.Error(exp);
                result.Message = ResponseMessage.Get(exp);
                result.Success = false;

                return result;
            }
        }

        [HttpPost("CreateUser")]
        public async Task<Result> CreateUser([FromForm] UserLogin newUser, IFormFile attachment)
        {
            var result = new Result();
            try
            {
                var user = new User();
                (user.Salt, user.Hash) = PasswordManager.HashPassword(newUser.Password);
                user.Roles = newUser.Roles;
                user.UserScreens = newUser.UserScreens;
                user.Username = newUser.Username;
                user.IsActive = true;

                if (attachment != null)
                {
                    var filename = await FileManager.UploadFile(Path.Combine(_env.WebRootPath, "user"), attachment);
                    user.Attachment = filename;
                }

                await _repository.InsertAsync(user);
                result.Success = true;
                result.Message = ResponseMessage.SUCCESSFULLY_CREATED;
            }
            catch (Exception exp)
            {
                _logError.Error(exp);
                result.Success = false;
                result.Message = ResponseMessage.SERVER_ERROR;
            }

            return result;
        }

        [HttpPut("updateUser/{id}")]
        public async Task<Result> UpdateUser(int id, User updatedUser)
        {
            var result = new Result();
            if (id != updatedUser.Id)
            {
                result.Message = ResponseMessage.BAD_REQUEST;
                result.Success = false;
                return result;
            }
            try
            {
                await _repository.UpdateAsync(updatedUser);

                result.Message = ResponseMessage.SUCCESSFULLY_UPDATED;
                result.Success = true;
                return result;
            }
            catch (Exception exp)
            {
                if (exp.Message == new NullReferenceException().Message)
                {
                    result.Message = "User" + ResponseMessage.NOT_FOUND;
                }

                _logError.Error(exp);
                result.Message = ResponseMessage.Get(exp);
                result.Success = false;

                return result;
            }
        }

        [HttpPost("ChangePassword")]
        public async Task<Result<IActionResult>> ChangePassword(ChangePassword newPassword)
        {
            var result = new Result<IActionResult>();
            try
            {
                if (!newPassword.Password.Equals(newPassword.ConfirmPassword))
                    throw new InvalidOperationException("Error: Provided password and confirm password did not match");

                var userId = int.Parse(User.FindFirstValue(CustomClaimTypes.UserId));
                await _repository.UpdatePasswordAsync(userId, newPassword.OldPassword, newPassword.Password);

                result.Message = ResponseMessage.SUCCESSFULLY_CHANGED;
                result.Success = true;

                return result;
            }
            catch (Exception ex)
            {
                _logError.Error(ex);

                result.Success = false;
                result.Message = ResponseMessage.Get(ex);

                return result;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _repository.DeleteUserAsync(id);
                return Ok(new Result { Message = "Successfully deleted" });
            }
            catch (Exception exp)
            {
                _logError.Error(exp);
                return Content(exp.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate(UserLogin user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _user = _repository.Validate(user);
                    if (_user.Username == null)
                    {
                        return Unauthorized();
                    }

                    var claims = new List<Claim>
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, _user.Username),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(CustomClaimTypes.UserId, _user.Id.ToString())
                    };

                    //foreach (var userRole in _user.Roles)
                    //    claims.Add(new Claim(ClaimTypes.Role, userRole.Role.Name));

                    foreach (var userScreen in _user.UserScreens)
                        claims.Add(new Claim(ClaimTypes.Role, userScreen.Screens.ScreenName));

                    var token = new JwtSecurityToken
                    (
                        issuer: _authConfiguration.Issuer,
                        audience: _authConfiguration.Audience,
                        claims: claims,
                        expires: DateTime.UtcNow.AddDays(60),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authConfiguration.SigningKey)),
                                SecurityAlgorithms.HmacSha256)
                    );

                    return Ok(new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        user_id = _user.Id,
                        username = _user.Username,
                        //roles = _user.RoleNames,
                        roles = _user.ScreenNames,
                        permissions = _user.Permissions,
                        photo = _user.Attachment
                    });
                }

                return BadRequest();
            }
            catch (Exception exp)
            {
                // keep log
                _logError.Error(exp);
                return BadRequest();
            }
        }
    }
}
