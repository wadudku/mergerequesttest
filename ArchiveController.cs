﻿using IDMS.Entity.Models;
using IDMS.Repository;
using IDMS.Utility;
using IDMS.Utility.StaticData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IDMS.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = ScreenName.All)]
    public class ArchiveController : ControllerBase
    {
        private readonly IRepository<Archive> _repository;
        private readonly ILogError _logError;
        private readonly IWebHostEnvironment _env;
        public ArchiveController(IRepository<Archive> _repository,
            IWebHostEnvironment env,
            ILogError _logError)
        {
            this._repository = _repository;
            this._logError = _logError;
            _env = env;
        }

        [HttpGet]
        public async Task<ListResult<Archive>> Get()
        {
            var result = new ListResult<Archive>()
            {
                Data = await _repository.GetAsync()
            };

            return result;
        }

        [HttpPost]
        public async Task<Result<Archive>> Post([FromForm]Archive item, IFormFile attachment)
        {
            var result = new Result<Archive>();

            if (!ModelState.IsValid)
            {
                result.Success = false;
                result.Message = ResponseMessage.BAD_REQUEST;
                return result;
            }

            try
            {
                if(attachment != null)
                {
                    var filename = await FileManager.UploadFile(Path.Combine(_env.WebRootPath, "archive"), attachment);
                    item.Attachment = filename;
                }
                await _repository.InsertAsync(item);
                result.Data = item;
                result.Message = ResponseMessage.SUCCESSFULLY_CREATED;
                return result;
            }
            catch (Exception exp)
            {
                // keep log;
                _logError.Error(exp);
                result.Message = ResponseMessage.Get(exp);
                result.Success = false;

                return result;
            }
        }

        [HttpPut("{id}")]
        public async Task<Result<Archive>> Put(int id, Archive item)
        {
            var result = new Result<Archive>();

            if (id != item.Id || !ModelState.IsValid)
            {
                result.Success = false;
                result.Message = ResponseMessage.BAD_REQUEST;
                return result;
            }
            try
            {
                await _repository.UpdateAsync(item);
                result.Data = item;
                result.Message = ResponseMessage.SUCCESSFULLY_UPDATED;
                return result;
            }
            catch (Exception exp)
            {
                // keep log
                _logError.Error(exp);
                result.Message = ResponseMessage.Get(exp);
                result.Success = false;

                return result;
            }
        }

        [HttpDelete("{id}")]
        public async Task<Result> Delete(int id)
        {
            var result = new Result();

            var item = await _repository.FindAsync(id);
            if (item == null)
            {
                result.Success = false;
                result.Message = ResponseMessage.NOT_FOUND;
                return result;
            }

            try
            {
                await _repository.DeleteAsync(item);
                FileManager.DeleteFile(Path.Combine(_env.WebRootPath, "archive", item.Attachment));
                result.Message = ResponseMessage.SUCCESSFULLY_DELETED;
                return result;
            }
            catch (Exception exp)
            {
                // keep log
                _logError.Error(exp);
                result.Message = ResponseMessage.Get(exp);
                result.Success = false;

                return result;
            }
        }
    }
}
